- 👋 Hi, I’m @sethlabadie
- 👀 I’m interested in changing the world through data science
- 🌱 I’m currently learning various Python packages, DevOps, Docker
- 💞️ I’m looking to collaborate on ... anything
- 📫 How to reach me: sethlabadie@gmail.com

<!---
sethlabadie/sethlabadie is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
